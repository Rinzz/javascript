function setValuesToIndexesInArray(arr) {
    let n = Number(arr[0]);
    let array = new Array(n).fill(0);

    for (let i = 1; i < arr.length; i++) {
        let input = arr[i].split(/\W{3}/);
        let index = Number(input[0]);
        array[index] = Number(input[1]);
    }

    for(let num of array) {
        console.log(num);
    }
}

setValuesToIndexesInArray(['5','0 - 3','3 - -1','4 - 2']);