function addOrRemoveElementOfArray(arr) {
    let array = [];
    for(let command of arr) {
        let infoCommand = command.split(' ');
        let action = infoCommand[0];

        switch (action) {
            case 'add':
                let number = Number(infoCommand[1]);
                array.push(number);
                break;
            case 'remove':
                let index = Number(infoCommand[1]);
                array.splice(index, 1);
                break;
        }
    }

    for(let item of array) {
        console.log(item);
    }
}

addOrRemoveElementOfArray(['add 3', 'add 5','add 7','remove 1']);

