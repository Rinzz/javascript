function printNumbersInReversedOrder(arr) {
    let numbers = arr.map(Number);
    numbers = numbers.reverse();
    for (let num of numbers){
        console.log(num);
    }
}

printNumbersInReversedOrder([1,2,3]);