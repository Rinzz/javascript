function multiplyValuesForKey(arr) {
    let pairs = {};
    for(let i = 0; i<arr.length-1; i++) {
        let inputInfo = arr[i].split(' ');
        let key = inputInfo[0];
        let value = inputInfo[1];
        if(!pairs.hasOwnProperty(key)) {
            pairs[key] = [];
        }
        pairs[key].push(value);
    }

    let key = arr[arr.length-1];
    if (pairs.hasOwnProperty(key)){
        for (let val of pairs[key]){
            console.log(val);
        }
    }
    else {
        console.log("None");
    }
}

multiplyValuesForKey(['3 bla','3 alb','2']);
multiplyValuesForKey(['3 test','3 test1','4 test2','4 test3','4 test5','4']);