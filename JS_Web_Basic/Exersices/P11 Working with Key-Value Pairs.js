function keyValuePairs(arr) {
    let pairs = {};

    for (let i = 0; i < arr.length-1; i++){
        let inputInfo = arr[i].split(' ');
        let key = inputInfo[0];
        let value = inputInfo[1];

        pairs[key] = value;
    }
    if (pairs.hasOwnProperty(arr[arr.length-1])){
        console.log(pairs[arr[arr.length-1]]);
    }
    else {
       console.log("None");
    }
}

keyValuePairs(['3 test', '3 test1', '4 test2', '4 test3', '4 test5', '4']);