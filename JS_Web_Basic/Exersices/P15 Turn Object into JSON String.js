function turnObjectIntoJSONString(arr) {
    let object = {};
    for(let pair of arr) {
        let pairInfo = pair.split(/\W{4}/);
        let key = pairInfo[0];
        let value = pairInfo[1];
        if(!isNaN(value)) {
            value = parseFloat(value);
        }
        object[key] = value;
    }

    let json = JSON.stringify(object);
    console.log(json);
}

turnObjectIntoJSONString(["name -> Angel","surname -> Georgiev","age -> 20","grade -> 6.00","date -> 23/05/1995","town -> Sofia"]);