function storingObjects(arr) {
    let arrayOfObjects = [];
    for(let string of arr) {
        let objectInfo = string.split(/\W{4}/);
        let name = objectInfo[0];
        let age = objectInfo[1];
        let grade = objectInfo[2];

        arrayOfObjects.push({
            Name: name,
            Age: age,
            Grade: grade
        });
    }
    console.log();

    for(let obj of arrayOfObjects) {
        for(let key of Object.keys(obj)) {
            console.log(`${key}: ${obj[key]}`);
        }
    }
}


storingObjects(["Pesho -> 13 -> 6.00","Ivan -> 12 -> 5.57","Toni -> 13 -> 4.90"]);