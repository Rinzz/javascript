function printNumberWithLoop(num) {
    let end = Number(num);

    for (let i = end; i > 0; i--){
        console.log(i);
    }
}

printNumberWithLoop('5');