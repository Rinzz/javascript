function  symmetricNumbers(num) {
    let n = Number(num[0]);
    let result = '';
    
    for (let i = 1; i <= n; i++){
        if (isSymmetric(i.toString())){
            result+=i + ' ';
        }
    }
    console.log(result);

    function isSymmetric(input) {
        for (let i = 0; i < input.length / 2; i++) {
            if (input[i] != input[input.length - i - 1]) {
                return false;
            }
        }

        return true;
    }
}

symmetricNumbers(['1000']);
