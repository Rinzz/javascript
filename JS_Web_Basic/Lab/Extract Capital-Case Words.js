function extractCapitalCaseWords(str) {
    let text = str.join(',');
    let words = text.split(/\W+/);
    let nonEmptyWords = words.filter(w => w.length > 0);
    let upWords = nonEmptyWords.filter(isUpperCase);
    console.log(upWords.join(", "));

    function isUpperCase(str) {
        return str === str.toUpperCase();
    }
}
extractCapitalCaseWords(['PHP, Java and HTML']);