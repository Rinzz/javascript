function calcDistance([v1, v2, time])
{
    let hours = time / 3600;
    let distance1 = v1 * hours;
    let distance2 = v2 * hours;
    console.log(Math.abs(distance1 - distance2)*1000);
}

calcDistance([0, 60, 3600]);